@extends('front-end.master')
@section('content')
<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Sản phẩm {{ $nameCate->name }}</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{ route('home') }}">Trang chủ</a> / <span>Loại sản phẩm</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<div class="col-sm-3">
					<ul class="aside-menu">
						@foreach($categories as $category)
						<li><a href="{{route('category', ['id' => $category->id])}}">{{ $category->name }}</a></li>
						@endforeach
					</ul>
				</div>
				<div class="col-sm-9">
					<div class="beta-products-list">
						<h4>Sản phẩm mới</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm thấy {{ count($products) }} sản phẩm</p>
							<div class="clearfix"></div>
						</div>

						<div class="row">
							@foreach($products as $product)
							<div class="col-sm-4">
								<div class="single-item">
									@if($product->promotion_price != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
									@endif
									<div class="single-item-header box-image-height">
										<a href="{{ route('product', ['id' => $product->id]) }}"><img class="thumb-keep-width" src="/image/product/{{ $product->image }}" alt=""></a>
									</div>
									<div class="single-item-body">
										<p class="single-item-title">{{ $product->name }}</p>
										<p class="single-item-price">
											@if($product->promotion_price != 0)
												<span class="flash-del">{{ number_format($product->unit_price)}}</span>
												<span class="flash-sale">{{ number_format($product->promotion_price) }}</span>
											@else
												<span class="flash-sale">{{ number_format($product->unit_price) }}</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										<a class="add-to-cart pull-left" href="{{ route('add-to-cart', ['id' => $product->id]) }}"><i class="fa fa-shopping-cart"></i></a>
										<a class="beta-btn primary" href="{{ route('product', ['id' => $product->id]) }}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div> <!-- .beta-products-list -->

					<div class="space50">&nbsp;</div>

					<div class="beta-products-list">
						<h4>Sản phẩm khác</h4>
						<div class="beta-products-details">
							<p class="pull-left">Có sản phẩm {{ count($otherProducts) }} đang được chú ý</p>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							@foreach($otherProducts as $otherProduct)
							<div class="col-sm-4">
								<div class="single-item">
									<div class="single-item-header box-image-height">
										@if($otherProduct->promotion_price != 0)
											<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<a href="{{ route('product', ['id' => $otherProduct->id]) }}"><img class="thumb-keep-width" src="/image/product/{{ $otherProduct->image }}" alt=""></a>
									</div>
									<div class="single-item-body">
										<p class="single-item-title">{{ $otherProduct->name }}</p>
										<p class="single-item-price">
											@if($otherProduct->promotion_price != 0)
												<span class="flash-del">{{ number_format($otherProduct->unit_price)}}</span>
												<span class="flash-sale">{{ number_format($otherProduct->promotion_price) }}</span>
											@else
												<span class="flash-sale">{{ number_format($otherProduct->unit_price) }}</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										<a class="add-to-cart pull-left" href="{{ route('add-to-cart', ['id' => $otherProduct->id]) }}"><i class="fa fa-shopping-cart"></i></a>
										<a class="beta-btn primary" href="{{ route('product', ['id' => $otherProduct->id]) }}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						<div class="space40">&nbsp;</div>

					</div> <!-- .beta-products-list -->
				</div>
			</div> <!-- end section with sidebar and main content -->


		</div> <!-- .main-content -->
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection