@extends('front-end.master')
@section('content')
<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="beta-products-list">
						<h4>Tìm kiếm</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm thấy {{count($products)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>

						@foreach($products as $key => $product)
							@if( $key%4 == 0 || $key == 0)
							<div class="row">
							@endif
								<div class="col-sm-3">
									<div class="single-item">
										@if($product->promotion_price != 0)
											<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<div class="single-item-header" style="height:320px;display:table-cell;vertical-align: middle;">
											<a href="{{ route('product', ['id' => $product->id]) }}"><img style="max-height:320px; width:100%; margin:auto" src=" image/product/{{ $product->image }}" alt=""></a>
										</div>
										<div class="single-item-body">
											<p class="single-item-title">{{ $product->name }}</p>
											<p class="single-item-price">
												@if($product->promotion_price != 0)
													<span class="flash-del">{{ number_format($product->unit_price)}}</span>
													<span class="flash-sale">{{ number_format($product->promotion_price) }}</span>
												@else
													<span class="flash-sale">{{ number_format($product->unit_price) }}</span>
												@endif
											</p>
										</div>
										<div class="single-item-caption">
											<a class="add-to-cart pull-left" href="{{ route('add-to-cart', ['id' => $product->id]) }}"><i class="fa fa-shopping-cart"></i></a>
											<a class="beta-btn primary" href="{{ route('product', ['id' => $product->id]) }}">Chi tiết <i class="fa fa-chevron-right"></i></a>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							@if( ($key+1)%4 == 0)
							</div>
							<div style="margin-bottom:20px"></div>
							@endif
						@endforeach
					</div> <!-- .beta-products-list -->
				</div>
			</div> <!-- end section with sidebar and main content -->


		</div> <!-- .main-content -->
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection