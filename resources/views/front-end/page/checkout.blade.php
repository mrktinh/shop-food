@extends('front-end.master')
@section('content')
<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Checkout</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb">
				<a href="index.html">Home</a> / <span>Checkout</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div id="content">
		
		<form action="{{ route('post-checkout') }}" method="post" class="beta-form-checkout">
			<input type="hidden" name="_token" value={{ csrf_token() }}>
			<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
				@if(Session::has('alert-' . $msg))
					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				@endif
			@endforeach
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h4>Billing Address</h4>
					<div class="space20">&nbsp;</div>

					<div class="form-block">
						<label for="your_first_name">Họ tên*</label>
						<input type="text" id="name" name="cus[name]" required>
					</div>

					<div class="form-block">
						<label for="adress">Địa chỉ*</label>
						<input type="text" id="adress" name="cus[address]" required>
					</div>

					<div class="form-block">
						<label for="email">Điện thoại*</label>
						<input type="number" id="email" name="cus[phone_number]" required>
					</div>

					<div class="form-block">
						<label for="email">Email*</label>
						<input type="email" id="email" name="cus[email]" required>
					</div>

					<div class="form-block">
						<label for="notes">Ghi chú</label>
						<textarea id="notes" name="cus[note]"></textarea>
					</div>
				</div>
				<div class="col-sm-6">
					@if(Session::has('cart'))
					<div class="your-order">
						<div class="your-order-head"><h5>Your Order</h5></div>
						<div class="your-order-body">
							<div class="your-order-item">
								<div>
								@foreach($cart as $product)
								<!--  one item	 -->
									<div class="media">
										<img width="35%" src="/image/product/{{$product['items']['image']}}" alt="" class="pull-left">
										<div class="media-body">
											<p class="font-large">{{ $product['items']['name'] }}</p>
											<span class="cart-item-amount">{{ $product['qty'] }}*
											<span>
												@if( $product['items']['promotion_price'] > 0)
												{{ number_format($product['items']['promotion_price']) }}
												@else
												{{ number_format($product['items']['unit_price']) }}
												@endif
											</span></span>
										</div>
									</div>
								@endforeach
								
								<!-- end one item -->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
								<div class="pull-left"><p class="your-order-f18">Total:</p></div>
								<div class="pull-right"><h5 class="color-black">{{ number_format(Session::get('cart')->totalPrice) }}</h5></div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="your-order-head"><h5>Payment Method</h5></div>
						
						<div class="your-order-body">
							<ul class="payment_methods methods">
								<li class="payment_method_bacs">
									<input id="payment_method_bacs" type="radio" class="input-radio" name="cus[payment_method]" value="cod" checked="checked" data-order_button_text="">
									<label for="payment_method_bacs">COD </label>
														
								</li>

								<li class="payment_method_cheque">
									<input id="payment_method_cheque" type="radio" class="input-radio" name="cus[payment_method]" value="atm" data-order_button_text="">
									<label for="payment_method_cheque">Chuyển khoản </label>
														
								</li>
								
								<li class="payment_method_paypal">
									<input id="payment_method_paypal" type="radio" class="input-radio" name="cus[payment_method]" value="paypal" data-order_button_text="Proceed to PayPal">
									<label for="payment_method_paypal">PayPal</label>
															
								</li>
							</ul>
						</div>

						<div class="text-center"><button type="submit" class="beta-btn primary" href="#">Checkout <i class="fa fa-chevron-right"></i></button></div>
					</div> <!-- .your-order -->
					@endif
				</div>
			</div>
		</form>
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection