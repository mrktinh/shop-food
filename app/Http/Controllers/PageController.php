<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Repositories\Interfaces\SlideInterface;
use App\Repositories\Interfaces\ProductInterface;
use App\Repositories\Interfaces\ProductTypeInterface;
use App\Repositories\Interfaces\CustomerInterface;
use App\Repositories\Interfaces\BillInterface;
use App\Repositories\Interfaces\BillDetailInterface;
use App\Models\Cart;

class PageController extends Controller
{
	private $slideRepo;
	private $productRepo;
    private $productTypeRepo;
    private $customerRepo;
    private $billRepo;
    private $billDetailRepo;

	public function __construct(
        SlideInterface $slideRepo,
        ProductInterface $productRepo,
        ProductTypeInterface $productTypeRepo,
        CustomerInterface $customerRepo,
        BillInterface $billRepo,
        BillDetailInterface $billDetailRepo
    )
	{
		$this->slideRepo = $slideRepo;
		$this->productRepo = $productRepo;
        $this->productTypeRepo = $productTypeRepo;
        $this->customerRepo = $customerRepo;
        $this->billRepo = $billRepo;
        $this->billDetailRepo = $billDetailRepo;
	}

    public function index()
    {
    	$slides = $this->slideRepo->all();
        $newProducts = $this->productRepo->getNewProduct();
        $saleProducts = $this->productRepo->getSaleProduct();
    	return view('front-end.page.home', [
            'slides' => $slides,
            'newProducts' => $newProducts,
            'saleProducts' => $saleProducts
        ]);
    }

    public function getCategory(Request $request)
    {
        $products = $this->productRepo->getProductByType($request->id);
        $otherProducts = $this->productRepo->getProductByOtherType($request->id);
        $categories = $this->productTypeRepo->all();
        $nameCate = $this->productTypeRepo->findById($request->id);
    	return view('front-end.page.category', [
            'products' => $products,
            'otherProducts' => $otherProducts,
            'categories' => $categories,
            'nameCate'   => $nameCate,
        ]);
    }

    public function getProduct(Request $request)
    {
        $product = $this->productRepo->findById($request->id);
        $relatedPros = $this->productRepo->getSomeProductByType($request->id, $product->id_type, 3);
    	return view('front-end.page.product', [
            'product' => $product,
            'relatedPros' => $relatedPros,
        ]);
    }

    public function getAbout()
    {
    	return view('front-end.page.about');
    }

    public function getContact()
    {
    	return view('front-end.page.contact');
    }

    public function getAddtoCart(Request $request)
    {
        $product = $this->productRepo->findById($request->id);
        $oldCart = Session::has('cart')? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $request->id);
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }

    public function getDeleteCart($id)
    {
        $oldCart = Session::has('cart')? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if(count($cart->items) > 0 ){
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->back();
    }

    public function getCheckout()
    {
        $cart = Session::get('cart');
        return view('front-end.page.checkout', ['cart' => ($cart)? $cart->items: null]);
    }

    public function postCheckout(Request $request)
    {
        try {
            DB::beginTransaction();
            $cart = Session::get('cart');

            $customerFields = ['name', 'gender', 'email', 'address', 'phone_number', 'note'];
            $dataCus = [];
            foreach($customerFields as $field){
                $dataCus[$field] = isset($request->cus[$field])? $request->cus[$field]: null;
            }
            $newCus = $this->customerRepo->create($dataCus);

            $billFields = [
                'id_customer' => $newCus->id,
                'date_order' => date('y-m-d'),
                'total'    => $cart->totalPrice,
                'payment' => $request->cus['payment_method'],
                'note'    => $request->cus['note'],
            ];
            $newBill = $this->billRepo->create($billFields);

            $billDetailDatas = [];
            foreach($cart->items as $key => $val) {
                $this->billDetailRepo->create([
                    'id_bill' => $newBill->id,
                    'id_product' => $key,
                    'quantity'   => $val['qty'],
                    'unit_price' => (int)($val['price']/$val['qty'])
                ]);
            }
            DB::commit();
            Session::forget('cart');
            return redirect()->back()->with('alert-success', 'Đặt hàng thành công');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('alert-danger', 'Có lỗi xảy ra');
        }
    }

    public function getSearch(Request $request)
    {
        $products = $this->productRepo->searchProByNameOrPrice($request->key);
        return view('front-end.page.search', ['products' => $products]);
    }
}
