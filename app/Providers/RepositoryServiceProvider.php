<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function boot()
    {

    }

    public function register()
    {
        $models = array(
            'Bill',
            'BillDetail',
            'Customer',
            'News',
            'Product',
            'ProductType',
            'Slide',
            'User',
        );

        foreach ($models as $model)
        {
            $this->app->bind("App\Repositories\Interfaces\\". $model . "Interface", "App\Repositories\Eloquents\\" . $model . "Repository");
        }
    }
}
