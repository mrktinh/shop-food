<?php

namespace App\Providers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use App\Models\ProductType;
use App\Models\Cart;

class ViewShareServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('front-end.header', function($view){
            $productTypes = ProductType::all();
            if(Session::has('cart')) {
                $oldCart = Session::get('cart');
                $cart = new Cart($oldCart);
                $view->with([
                    'productTypes' => $productTypes,
                    'product_cart' => $cart->items,
                    //'cart' => Session::get('cart'),
                    // 'totalPrice'  => $cart->totalPrice,
                    // 'totalQty'  => $cart->totalQty,
                ]);
            } else {
                $view->with([ 'productTypes' => $productTypes ]);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
