<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BillDetail;

class Bill extends Model
{
    protected $table = 'bills';

    protected $fillable = ['id_customer', 'date_order', 'total', 'payment', 'note'];

    public function bill_detail()
    {
    	return $this->hasMany('App\BillDetail', 'id_bill', 'id');
    }

    public function customer()
    {
    	return $this->belongsTo('App\Customer', 'id_customer', 'id');
    }

    public function product()
    {
    	return $this->belongsToMany('App\Product', BillDetail::getTableName(), 'id_bill', 'id_product' );
    }
}
