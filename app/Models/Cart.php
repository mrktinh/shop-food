<?php
namespace App\Models;

class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;


    public function __construct($oldCart)
    {
        if($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($item, $id)
    {
     $cart = ['qty' => 0,'price' => ($item->promotion_price > 0)? $item->promotion_price: $item->unit_price, 'items' => $item];
     if($this->items) {
        if(array_key_exists($id, $this->items)){
            $cart = $this->items[$id];
        }
     }
     $cart['qty']++;
     $cart['price'] = $item->unit_price * $cart['qty'];
     $this->items[$id] = $cart;
     $this->totalQty++;
     $this->totalPrice += ($item->promotion_price > 0)? $item->promotion_price: $item->unit_price;
    }

    public function decreaseByOne($id)
    {
        $this->items[$id]['qty']--;
        $this->items[$id]['price'] -= $this->items[$id]['items']['price'];
        $this->totalQty--;
        $this->totalPrice -= $this->items[$id]['item']['price'];
        if($this->items[$id]['qty'] <= 0) {
            unset($this->items[$id]);
        }
    }

    public function removeItem($id)
    {
        $this->totalQty -= $this->items[$id]['qty'];
        $this->totalPrice -= $this->items[$id]['price'];
        unset($this->items[$id]);
    }
}
