<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BillDetail;

class Product extends Model
{
    protected $table = 'products';

    public function product_type()
    {
    	return $this->belongsTo('App\ProductType', 'id_type', 'id');
    }

    public function bill_detail()
    {
    	return $this->hasMany('App\BillDetail', 'id_product', 'id');
    }

    public function bill()
    {
    	return $this->belongsToMany('App\Bill', BillDetail::getTableName(), 'id_product', 'id_bill');
    }

}
