<?php
namespace App\Repositories\Eloquents;

use App\Models\Slide;
use App\Repositories\Interfaces\SlideInterface;
use App\Repositories\Eloquents\Repository;

class SlideRepository extends Repository implements SlideInterface
{

    public function __construct(Slide $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
