<?php
namespace App\Repositories\Eloquents;

use App\Models\BillDetail;
use App\Repositories\Interfaces\BillDetailInterface;
use App\Repositories\Eloquents\Repository;

class BillDetailRepository extends Repository implements BillDetailInterface
{

    public function __construct(BillDetail $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
