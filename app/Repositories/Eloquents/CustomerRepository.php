<?php
namespace App\Repositories\Eloquents;

use App\Models\Customer;
use App\Repositories\Interfaces\CustomerInterface;
use App\Repositories\Eloquents\Repository;

class CustomerRepository extends Repository implements CustomerInterface
{

    public function __construct(Customer $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
