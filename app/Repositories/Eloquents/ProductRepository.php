<?php
namespace App\Repositories\Eloquents;

use App\Models\Product;
use App\Repositories\Interfaces\ProductInterface;
use App\Repositories\Eloquents\Repository;
use Illuminate\Support\Facades\DB;

class ProductRepository extends Repository implements ProductInterface
{

    public function __construct(Product $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }

    public function getNewProduct()
    {
    	return $this->model->where('new', 1)
    		->orderBy('created_at', 'desc')
    		->take(4)
    		->get();
    }

    public function getSaleProduct()
    {
    	return $this->model->where('promotion_price', '>', 0)
    		->orderBy('created_at', 'desc')
    		->take(8)
    		->get();
    }

    public function getProductByType($id)
    {
        return $this->model->where('id_type', $id)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function getProductByOtherType($id)
    {
        return $this->model->where('id_type', '<>', $id)
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get();
    }

    public function getSomeProductByType($id, $idType, $number)
    {
        return $this->model->where('id_type', $idType)
            ->where('id', '<>', $id)
            ->orderBy('created_at', 'desc')
            ->take($number)
            ->get();
    }

    public function searchProByNameOrPrice($key)
    {
        return $this->model->where('name', 'like', '%'.$key.'%')
            ->orWhere('unit_price', $key)
            ->get();
    }
}
