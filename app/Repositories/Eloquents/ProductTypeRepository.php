<?php
namespace App\Repositories\Eloquents;

use App\Models\ProductType;
use App\Repositories\Interfaces\ProductTypeInterface;
use App\Repositories\Eloquents\Repository;
use Illuminate\Support\Facades\DB;

class ProductTypeRepository extends Repository implements ProductTypeInterface
{

    public function __construct(ProductType $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
