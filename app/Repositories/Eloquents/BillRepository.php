<?php
namespace App\Repositories\Eloquents;

use App\Models\Bill;
use App\Repositories\Interfaces\BillInterface;
use App\Repositories\Eloquents\Repository;

class BillRepository extends Repository implements BillInterface
{

    public function __construct(Bill $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
