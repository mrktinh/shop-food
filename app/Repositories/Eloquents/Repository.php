<?php

namespace App\Repositories\Eloquents;

class Repository {

    /**
    * Eloquent model
    */
    protected $model;

    /**
    * @param $model
    */
    function __construct($model)
    {
        $this->model = $model;
    }

    public function all($columns = array('*'))
    {
        return $this->model->all($columns);
    }

    public function paginateList($per=10)
    {
        return $this->model->orderBy('id', 'desc')->paginate($per);
    }

    public function pluck($column, $sortColumn = null, $direction = 'asc', $key = null)
    {
        if ($sortColumn) {
            return $this->model->orderBy($sortColumn, $direction)->pluck($column, $key);
        } else {
            return $this->model->pluck($column, $key);
        }
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($data, $key, $value)
    {
        $obj = $this->model->where($key, $value);
        return $obj->update($data);
    }

    public function destroy($key, $value)
    {
        return $this->model->where($key, $value)->delete();
    }

    public function findById($id, $columns = array('*'))
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function findBy($key, $value)
    {
        return $this->model->where($key, $value)->first();
    }

    public function findAllBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function pluckBy($key, $value, $column, $sortColumn = 'id', $direction = 'asc') {
        return $this->model->orderBy($sortColumn, $direction)
            ->where($key, $value)
            ->pluck($column);
    }
}
