<?php
namespace App\Repositories\Interfaces;

interface ProductInterface
{
	public function getNewProduct();
	public function getSaleProduct();
	public function getProductByType($id);
	public function getProductByOtherType($id);
	public function getSomeProductByType($id, $idType, $number);
}
