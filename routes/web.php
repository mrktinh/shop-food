<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PageController@index')->name('home');
Route::get('category/{id}', 'PageController@getCategory')->name('category');
Route::get('product/{id}', 'PageController@getProduct')->name('product');
Route::get('about', 'PageController@getAbout')->name('about');
Route::get('contact', 'PageController@getContact')->name('contact');
Route::get('checkout', 'PageController@getCheckout')->name('get-checkout');
Route::post('checkout', 'PageController@postCheckout')->name('post-checkout');
Route::get('add-to-cart/{id}', 'PageController@getAddtoCart')->name('add-to-cart');
Route::get('delete-cart/{id}', 'PageController@getDeleteCart')->name('remove-cart');
Route::get('search', 'PageController@getSearch')->name('get-search');
Route::get('login', 'Auth\LoginController@getLogin')->name('get-login');
Route::get('register', 'Auth\RegisterController@getRegister')->name('get-register');